# txtar - very simple, multi-text-file archive

In his [go testing by example](https://www.youtube.com/watch?v=1-o-iJlL4ak)
talk, (2023) Russ Cox mentioned [x/tools/txtar](https://pkg.go.dev/golang.org/x/tools/txtar),
a very simple text base archive for encapsulating multiple test files.

Their intention was to maintain sets of inputs and outputs for test cases,
bundled in a single, human readable text file per test case.

However, he also mentioned the "trick" of adding an `-update` flag to your test
code, without mentioning how he actually generated his `txtar` files.
(There is no code in the `txtar` package for generating `txtar` files).

It would of course be much nicer if txtar took care of this in a more
freindly manner.

## Goals

- [x] provide access to archived files by name
- [x] allowing files to be (re-)written in the archive

## Update: 2024-12
  - [https://golang.org/x/tools/txtar](https://golang.org/x/tools/txtar)
    now has a FS wrapper
  - sadly, google's FS implementation is deliberately _**read-only**_:
    > File system operations are read only.
    > _Modifications to the underlying `*Archive` may **race**_.
  - while race-conditions may be a _possibility_, if you are using your
    archives in a racey way (concurrent writes?!), you are asking for trouble.
  - this project has been updated to include google's FS implementation, but my
    extension also remains.

## Extensions

`func (a *Archive) HasFile(name string) bool`

HasFile returns true if a file with the provided name exists.

`func (a *Archive) ReadFileData(name string) []byte`

ReadFileData returns the contents of a file in the archive.

If no files in the archive have the name provided, then an empty []byte
slice is returned.

`func (a *Archive) WriteFileData(name string, data []byte)`

WriteFileData replaces a file in the archive, or appends a new file to the
end of the archive if no file with the provided name exists in the archive
yet.

## Example

e.g.: `foo_test.go`
```golang
package main

var update = flag.Bool("update",false,"rewrite test expectations")

func TestFoo(t *testing.T) {
    files, _ := filepath.Glob("testdata/*.txt")
    if len(files) == 0 {
      t.Fatalf("no test data")
    }

    for _, file := range files {
        t.Run(filepath.Base(file), func(t *testing.T){
            a, err := txtar.ParseFile(file)
            if err != nil {
                t.Fatal(err)
            }

            // NEW: access archived files by name instead of a.Files[0].Data
            given := a.ReadFileData("given")
            want  := a.ReadFileData("wanted")
            got   := foo(given)

            if *update {
                // assume that we got what we wanted and re-write the archive
                // with the new expected output as the 'wanted' file in the
                // archive

                // NEW: update contents of "wanted" in the archive
                a.WriteFileData("wanted", got)
                want = got // make the test pass

                // NEW: format the archive and write it back to disk
                a.WriteFile()
            }

            if got != want {
                t.Errorf("unexpected result")
            }
        })
    }
}
```

