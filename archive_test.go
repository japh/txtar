// Copyright 2018 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package txtar

import (
	"bytes"
	"fmt"
	"reflect"
	"testing"
)

func TestParse(t *testing.T) {
	var tests = []struct {
		name   string
		text   string
		parsed *Archive
	}{
		{
			name: "basic",
			text: `comment1
comment2
-- file1 --
File 1 text.
-- foo ---
More file 1 text.
-- file 2 --
File 2 text.
-- empty --
-- noNL --
hello world
-- empty filename line --
some content
-- --`,
			parsed: &Archive{
				Comment: []byte("comment1\ncomment2\n"),
				Files: []File{
					{"file1", []byte("File 1 text.\n-- foo ---\nMore file 1 text.\n")},
					{"file 2", []byte("File 2 text.\n")},
					{"empty", []byte{}},
					{"noNL", []byte("hello world\n")},
					{"empty filename line", []byte("some content\n-- --\n")},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := Parse([]byte(tt.text))
			if !reflect.DeepEqual(a, tt.parsed) {
				t.Fatalf("Parse: wrong output:\nhave:\n%s\nwant:\n%s", shortArchive(a), shortArchive(tt.parsed))
			}
			text := Format(a)
			a = Parse(text)
			if !reflect.DeepEqual(a, tt.parsed) {
				t.Fatalf("Parse after Format: wrong output:\nhave:\n%s\nwant:\n%s", shortArchive(a), shortArchive(tt.parsed))
			}
		})
	}
}

func TestFormat(t *testing.T) {
	var tests = []struct {
		name   string
		input  *Archive
		wanted string
	}{
		{
			name: "basic",
			input: &Archive{
				Comment: []byte("comment1\ncomment2\n"),
				Files: []File{
					{"file1", []byte("File 1 text.\n-- foo ---\nMore file 1 text.\n")},
					{"file 2", []byte("File 2 text.\n")},
					{"empty", []byte{}},
					{"noNL", []byte("hello world")},
				},
			},
			wanted: `comment1
comment2
-- file1 --
File 1 text.
-- foo ---
More file 1 text.
-- file 2 --
File 2 text.
-- empty --
-- noNL --
hello world
`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result := Format(tt.input)
			if string(result) != tt.wanted {
				t.Errorf("Wrong output. \nGot:\n%s\nWant:\n%s\n", string(result), tt.wanted)
			}
		})
	}
}

func shortArchive(a *Archive) string {
	var buf bytes.Buffer
	fmt.Fprintf(&buf, "comment: %q\n", a.Comment)
	for _, f := range a.Files {
		fmt.Fprintf(&buf, "file %q: %q\n", f.Name, f.Data)
	}
	return buf.String()
}

func TestReadFileData(t *testing.T) {
	var tests = []struct {
		name     string
		archive  *Archive
		readName string
		wanted   string
	}{
		{
			name: "read from empty archive",
			archive: &Archive{
				Comment: []byte{},
				Files:   []File{},
			},
			readName: "foo",
			wanted:   "",
		},
		{
			name: "read an existing file from an archive",
			archive: &Archive{
				Comment: []byte{},
				Files: []File{
					{"foo", []byte("I am a teapot")},
				},
			},
			readName: "foo",
			wanted:   "I am a teapot",
		},
		{
			name: "read one of many files in an archive",
			archive: &Archive{
				Comment: []byte{},
				Files: []File{
					{"foo", []byte("I am a teapot")},
					{"bar", []byte("I am a coffee pot")},
				},
			},
			readName: "foo",
			wanted:   "I am a teapot",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.archive.ReadFileData(tt.readName)
			if string(got) != tt.wanted {
				t.Errorf("Wrong output. \nGot:\n%s\nWant:\n%s\n", string(got), tt.wanted)
			}
		})
	}
}

func TestWriteFileData(t *testing.T) {
	var tests = []struct {
		name      string
		archive   *Archive
		writeName string
		writeData string
		wanted    string
	}{
		{
			name: "write to an empty archive",
			archive: &Archive{
				Comment: []byte{},
				Files:   []File{},
			},
			writeName: "file1",
			writeData: "File 1 text.\n",
			wanted: `-- file1 --
File 1 text.
`,
		},
		{
			name: "append new file to the end of an archive",
			archive: &Archive{
				Comment: []byte{},
				Files: []File{
					{"file1", []byte("File 1 text.\n")},
				},
			},
			writeName: "file2",
			writeData: "File 2 text.\n",
			wanted: `-- file1 --
File 1 text.
-- file2 --
File 2 text.
`,
		},
		{
			name: "overwrite existing file in an archive",
			archive: &Archive{
				Comment: []byte{},
				Files: []File{
					{"file1", []byte("File 1 text.\n")},
					{"file2", []byte("File 2 text.\n")},
					{"file3", []byte("File 3 text.\n")},
				},
			},
			writeName: "file2",
			writeData: "NEW File 2 text.\nUpdated and much improved.\n",
			wanted: `-- file1 --
File 1 text.
-- file2 --
NEW File 2 text.
Updated and much improved.
-- file3 --
File 3 text.
`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.archive.WriteFileData(tt.writeName, []byte(tt.writeData))
			got := Format(tt.archive)
			if string(got) != tt.wanted {
				t.Errorf("Wrong output. \nGot:\n%s\nWant:\n%s\n", string(got), tt.wanted)
			}
		})
	}
}

func TestFileExistenceTest(t *testing.T) {
	var tests = []struct {
		name    string
		archive *Archive
		hasFile string
		want    bool
	}{
		{
			name: "an empty archive has no files",
			archive: &Archive{
				Comment: []byte{},
				Files:   []File{},
			},
			hasFile: "file1",
			want:    false,
		},
		{
			name: "check for an existing file",
			archive: &Archive{
				Comment: []byte{},
				Files: []File{
					{"file1", []byte("File 1 text.\n")},
				},
			},
			hasFile: "file1",
			want:    true,
		},
		{
			name: "check for a missing file",
			archive: &Archive{
				Comment: []byte{},
				Files: []File{
					{"file1", []byte("File 1 text.\n")},
				},
			},
			hasFile: "file2",
			want:    false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.archive.HasFile(tt.hasFile)
			if got != tt.want {
				t.Errorf("Wrong output. \nGot:  %v\nWant: %v\n", got, tt.want)
			}
		})
	}
}
